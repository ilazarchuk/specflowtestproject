﻿Feature: Divide
       In order to avoid silly mistakes
       As a math idiot
       I want to be told the sum of two numbers

@mytag
Scenario: Divide two numbers
       Given I have entered 150 into the calculator
       And I have also entered 50 into the calculator
       When I press divide
       Then the result should be 3 on the screen
