using TechTalk.SpecFlow;
using CalculatorProject;
using NUnit.Framework;

namespace SpecFlowTestProject
{
    [Binding]
    public class CalculatorSteps
    {
        private readonly Calculator calculator = new Calculator();
        private int result;
        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int number)
        {
            calculator.FirstNumber = number;
        }

        [Given(@"I have also entered (.*) into the calculator")]
        public void GivenIHaveAlsoEnteredIntoTheCalculator(int number)
        {
            calculator.SecondNumber = number;
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            result = calculator.Add();
        }

        [When(@"I press divide")]
        public void WhenIPressDivide()
        {
            result = calculator.Divide();
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int expectedResult)
        {
            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}